
var controllers = {};

controllers.LoginController = function ($scope, $http) {


	/**/
	$scope.showLoginModal = false;
	$scope.toggleLoginModal = function(){
		$scope.showLoginModal = !$scope.showLoginModal;
	};

	$scope.showVerificationModal = false;
	$scope.toggleVerificationModal = function(){
		$scope.showVerificationModal = !$scope.showVerificationModal;
	};
	$scope.showRegistrationModal = false;
	$scope.toggleRegistrationModal = function() {
		$scope.showRegistrationModal = !$scope.showRegistrationModal;
	}

	setInfoMessage = function(msg) {
		$scope.infoMessage = msg;
		$scope.errorMessage = "";
	}

	setErrorMessage = function(msg) {
		$scope.errorMessage = msg;
		$scope.infoMessage = "";
	}

	$scope.verification = {};
	$scope.verification.code = "";
	$scope.verification.username = "";

	$scope.verify = function() {
		setInfoMessage("Verifying ...");
		$http.defaults.headers.post["Content-Type"] = "application/json";

		$http({url: '/verify',
			method: 'POST',
			headers: {'Content-Type': 'application/json'},
			data: 
			{
				verificationcode: $scope.verification.code,
				username: $scope.verification.username
			}
		}).
		success(function(data, status, headers, config) {
			if(data.error) {        
				setErrorMessage(data.error);        
				return
			} else {
				setInfoMessage(data.msg);
				$scope.toggleLoginModal();
				$scope.toggleVerificationModal();
			}
		}).
		error(function(data, status, headers, config) {
			console.log(data);
		});
	}

	$scope.user={};
	$scope.user.username="";
	$scope.user.password="";

	$scope.login = function() {
		setInfoMessage("Loggin in...");
		$http.defaults.headers.post["Content-Type"] = "application/json";

		$http({url: '/login?',
			method: 'GET',
			headers: {'Content-Type': 'application/json'},
			params: {
				username: $scope.user.username,
				password: $scope.user.password
			}
//   data: 
//   {
//    username: $scope.user.username,
//    password: $scope.user.password
// }
}).
		success(function(data, status, headers, config) {
			console.log(data)
			if(data.error) {
				setErrorMessage(data.error)        
				return
			}
// window.location.href = window.location.href+"login"
}).
		error(function(data, status, headers, config) {
		});
	}


	$scope.createAccount = function() {

		$http.defaults.headers.post["Content-Type"] = "application/json";

		$http({url: '/addUser',
			method: 'POST',
			headers: {'Content-Type': 'application/json'},
			data: {
				username: $scope.registration.user.username,
				name: $scope.registration.user.name,
				email: $scope.registration.user.email,
				password: $scope.registration.user.password
			}
//   data: 
//   {
//    username: $scope.registration.user.username,
//    name: $scope.registration.user.name,
//    email: $scope.registration.user.email,
//    password: $scope.registration.user.password
// }
}).
		success(function(data, status, headers, config) {

			if (data.error) {
				setErrorMessage(data.error);
				return;
			} else {
				setInfoMessage(data.msg);
				$scope.toggleRegistrationModal();
				$scope.toggleVerificationModal();
			}

			$scope.showVerification();
		}).
		error(function(data, status, headers, config) {
			$scope.registrationMessage = data
		});
	}
};

controllers.myCtrl = function($scope, $http){
	$scope.options = {
		chart: {
			type: 'lineChart',
			height: 450,
/*margin : {
height: 200,
width: 400,
margin : {

top: 20,
right: 40,
bottom: 60,
left: 55
},*/
x: function(d){ return d.x; },
//y: function(d){ return 100.0*((d.y-d.y0)/d.y0); },
y: function(d) { return d.y},
//yDomain: [0, 100],
useInteractiveGuideline: true,
dispatch: {
	stateChange: function(e){ 
		console.log("stateChange"); 
	},
	changeState: function(e){ 
		console.log("changeState"); 
	},
	tooltipShow: function(e){ 
		console.log("tooltipShow"); 
	},
	tooltipHide: function(e){ 
		console.log("tooltipHide"); 
	}
},
xAxis: {
	axisLabel: 'Date',
	tickFormat: function(d) {
		return d3.time.format('%b %d %Y')(new Date(d));
	},
	axisLabelDistance: 30
},
yAxis: {
	axisLabel: 'Percentage swing (of daily average) (%)',
	tickFormat: function(d){
		return d3.format('.02f')(d);
	},
	axisLabelDistance: 30
},
callback: function(chart){
	console.log("!!! lineChart callback !!!");
}
},
title: {
	enable: true,
	text: 'DAily percentage swing'
},
subtitle: {
	enable: true,
	text: 'Percentage daily swing',
	css: {
		'text-align': 'center',
		'margin': '10px 13px 0px 7px'
	}
},
caption: {
	enable: false,
	html: '<b>Figure 1.</b> Lorem ipsum dolor sit amet, at eam blandit sadipscing, <span style="text-decoration: underline;">vim adhuc sanctus disputando ex</span>, cu usu affert alienum urbanitas. <i>Cum in purto erat, mea ne nominavi persecuti reformidans.</i> Docendi blandit abhorreant ea has, minim tantas alterum pro eu. <span style="color: darkred;">Exerci graeci ad vix, elit tacimates ea duo</span>. Id mel eruditi fuisset. Stet vidit patrioque in pro, eum ex veri verterem abhorreant, id unum oportere intellegam nec<sup>[1, <a href="https://github.com/krispo/angular-nvd3" target="_blank">2</a>, 3]</sup>.',
	css: {
		'text-align': 'justify',
		'margin': '10px 13px 0px 7px'
	}
}
};

$scope.symbolEntered = function() {

	$scope.data = [];

	var responsePromise = $http({url: '../get_historical_percentage_daily_swing',
		method: 'POST',
		headers: {'Content-Type': 'application/json'},
		data: 
		{
			symbol: $scope.symbol,
// company: typeof $scope.sellsymbol.company == 'undefined' ? "":$scope.sellsymbol.company,
// shares: $scope.numSharesToSell,
// pricepershare: $scope.pricepersharesell,
// total:  $scope.proceedsFromSelling
}
})

	responsePromise.then( function(response) {
		$scope.data = []
		var swing = [];

		var hasData = false;
		for (var j in response.data) {
			swing.push({x: Date.parse(response.data[j].Date), y: parseFloat(response.data[j].Swing)});
			hasData = true;

		}
		if (hasData){
			$scope.data.push({values: swing, key: response.data[0].Symbol, color: '#'+(Math.random()*0xFFFFFF<<0).toString(16)});
		}

	}
	,function (response) {
		console.log(response);
	});
}
};

mymodal.controller(controllers);
