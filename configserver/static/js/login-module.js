var mymodal = angular.module('stockApp'
	,['ngMessages', 'nvd3'])
;

mymodal.config(function($interpolateProvider) {
	$interpolateProvider.startSymbol('{[{').endSymbol('}]}');
});